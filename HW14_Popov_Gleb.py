"""
1. Напишите декоратор, замеряющий время выполнения функции.
Декоратор должен вывести на экран время выполнения задекорированной функции
"""


def time_func(func):
    """Декоратор, определяющий время выполнения функции."""
    import time

    def wrapper(*args, **kwargs):
        start = time.time()
        return_value = func(*args, **kwargs)
        end = time.time()
        print(f'Время выполнения: {end - start} секунд.')
        return return_value
    return wrapper


class Point:
    """Класс, определяющий координаты точки для линии"""
    def __init__(self, x_coord, y_coord):
        if not isinstance(x_coord, (int, float)) and isinstance(y_coord, (int, float)):
            raise TypeError('Нечисловое значение используемых данных для класса Point.')
        self.x = x_coord
        self.y = y_coord


class Line:
    """Класс, в котором определяются две точки p1 и p2 на основе класса Point."""
    _p1 = None
    _p2 = None

    def __init__(self, p1, p2):
        self.p1 = p1
        self.p2 = p2

    def read_point1(self):
        return self._p1

    def write_point1(self, start):
        if not isinstance(start, Point):
            return TypeError('Указаны объекты отличные от класса Point.')
        self._p1 = start

    p1 = property(read_point1, write_point1)

    def read_point2(self):
        return self._p2

    def write_point2(self, end):
        if not isinstance(end, Point):
            return TypeError('Указаны объекты отличные от класса Point.')
        self._p2 = end

    p2 = property(read_point2, write_point2)

    @time_func
    def line_length(self):
        """Метод, определяющий длину линии."""
        res = ((self.p1.x - self.p2.x) ** 2 + (self.p1.y - self.p2.y) ** 2) ** 0.5
        return res
    line_lenght_attr = property(line_length)


"""line1 - объект, в котором задаются координаты двух точек для определения длины линии."""
line1 = Line(Point(2, 2), Point(20, 20))
print(line1.p1)
print(line1.p2)
print(line1.line_lenght_attr)


class Triangle:
    """Класс, в котором определяются вершины треугольника - apex, на основе класса TrianglePoint."""
    apex1 = Point(0, 0)
    apex2 = Point(0, 0)
    apex3 = Point(0, 0)

    def __init__(self, apex1, apex2, apex3):
        if not isinstance(apex1, Point) and isinstance(apex2, Point) and isinstance(apex3, Point):
            raise TypeError('Указаны объекты отличные от класса Point.')
        self.apex1 = apex1
        self.apex2 = apex2
        self.apex3 = apex3

    @time_func
    def triangle_creator(self):
        """Метод класса Triangle, определяющий периметр треугольника по вершинам apex."""
        plane1 = ((self.apex1.x - self.apex2.x) ** 2 + (self.apex1.y - self.apex2.y) ** 2) ** 0.5
        plane2 = ((self.apex2.x - self.apex3.x) ** 2 + (self.apex2.y - self.apex3.y) ** 2) ** 0.5
        plane3 = ((self.apex3.x - self.apex1.x) ** 2 + (self.apex3.y - self.apex1.y) ** 2) ** 0.5
        triangle = plane1 + plane2 + plane3
        return triangle
    triangle_creator_attr = property(triangle_creator)

    def __iter__(self):
        print('Начало итератора')
        self._current = 0
        return self

    def __next__(self):
        print('Следующий итерируемый элемент')
        self._current += 1
        if self._current - 1 == 0:
            return self.apex1
        elif self._current - 1 == 1:
            return self.apex2
        elif self._current - 1 == 2:
            return self.apex3
        else:
            raise StopIteration


triangle_cord = Triangle(Point(1, 1), Point(4, 4), Point(6, 1))
"""triangle_cord - объект, в котором задаются координаты трёх точек для определения площади треугольника."""
print(triangle_cord.triangle_creator_attr)

for i in triangle_cord:
    print(i)
